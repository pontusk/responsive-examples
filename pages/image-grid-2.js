import { imgUrl } from "../helpers";

export default function ImageGrid() {
  const keywords = [
    'kittens',
    'pancakes',
    'beer',
    'volfram',
    'irregular',
    'excelent',
    'lard',
    'mood',
    'fork'
  ]
  return (
    <div>
      <main className="has-background-primary-light">
        <div className="box">
          <div className="image-holder">
            {Array(9).fill(`${imgUrl}`).map((url,i) => (
              <div className="image" key={i} style={{ backgroundImage: `url(${url}${keywords[i]})`}}/>
            ))}
          </div>
          <h2 className="has-text-primary">Lorem ipsum</h2>
        </div>
      </main>
      <style jsx>{`
        .box {
          width: 80vw;
          height: 80vh;
          display: grid;
          grid-template-rows: 1fr auto;
        }
        main {
          display: flex;
          justify-content: center;
          align-items: center;
        }
        h2 {
          font-size: 3rem;
          text-align: center;
          margin: 1rem 0 1rem 0;
        }
        .image-holder {
          display: grid;
          grid-template-columns: repeat(5, 1fr);
          grid-gap: 1rem;
        }
        .image {
          background-size: cover;
        }

      `}</style>
    </div>
  )
}