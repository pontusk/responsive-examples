import Head from 'next/head'
import Link from 'next/link'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Responsive design</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Link href="/login-1"><a className="button is-link is-medium">Login Page With Img</a></Link>
        <Link href="/login-2"><a className="button is-link is-medium">Login Page With BgImage</a></Link>
        <Link href="/image-grid-1"><a className="button is-link is-medium">Image Grid With Img</a></Link>
        <Link href="/image-grid-2"><a className="button is-link is-medium">Image Grid With BgImage</a></Link>
      </main>
      <style jsx>{`
        main {
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          padding: 2rem;
        }  
        .button {
          width: 100%;
          margin: 0.5rem 0 0.5rem 0;
        }
      `}</style>
    </div>
  )
}
