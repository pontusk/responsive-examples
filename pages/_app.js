import '../styles/globals.css'
import '../styles/bulma.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
