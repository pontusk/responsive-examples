import { imgUrl } from "../helpers";

export default function Login() {
  return (
    <div>
      <main>
        <div className="headerImage">
          <img src={imgUrl}/>
          <h1>TITLE</h1>
        </div>
        <div className="content has-background-primary-light">
          <label className="label has-text-primary">
            User Name
            <input className="input is-medium is-primary" type="text" placeholder="User Name"/>
          </label>
          <label className="label has-text-primary">
            Password
            <input className="input is-medium is-primary" type="text" placeholder="Password"/>
          </label>
          <div className="buttons">
            <button className="button is-primary is-medium">Log in</button>
            <button className="button is-medium">Sign up</button>
          </div>
        </div>
      </main>
      <style jsx>{`
        main {
          display: flex;
          flex-direction: column;
        }
        h1 {
          position: absolute;
          font-weight: bold;
          font-size: 6rem;
          color: white;
        }
        .headerImage {
          display: flex;
          justify-content: center;
          flex: 1;
          width: 100%;
        }
        img {
          width: 100%;
          height: 100%;
        }
        .content {
          flex: 1;
          padding: 2rem;
          display: flex;
          flex-direction: column;
          justify-content: space-around;
        }
        .buttons {
          display: flex;
        }
        .button {
          flex: 1;
        }
      `}</style>
    </div>
  );
}
